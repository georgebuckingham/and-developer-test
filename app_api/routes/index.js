/**
 * 
 * Created by georgebuckingham on 04/05/2016.
 */

var express = require('express');
var router = express.Router();
var apiCtrl = require('../controllers/api');

router.get('/search', apiCtrl.search);

module.exports = router;
