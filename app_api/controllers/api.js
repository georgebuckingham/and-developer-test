/**
 * 
 * Created by georgebuckingham on 04/05/2016.
 */

require('express');
var request = require('request');
var config = require('../../config.json');
var extend = require('extend');

var defaultSettings = function() {
    return {
        'baseUrl': config.foursquare_endpoint,
        'qs': {
            'client_id': config.client_id,
            'client_secret': config.client_secret,
            'v': config.version
        }
    };
};

module.exports.search = function(req, res) {
    var localSettings = defaultSettings();
    extend(localSettings, {
        uri: '/venues/explore'
    });
    extend(localSettings.qs, {
        near: req.query.term,
        offset: req.query.offset || 0,
        limit: req.query.limit || 4
    });
    request.get(localSettings,
        function(error, response, body) {
            if (error) {
                res.status(400);
            } else {
                var responseObj = JSON.parse(body);
                if (responseObj.meta && responseObj.meta.code == 400) {
                    res.status(400);
                    res.json(responseObj.meta);
                    return;
                } else {
                    res.status(200);
                    res.json(JSON.parse(body).response.groups);
                }
            }
        })
};
