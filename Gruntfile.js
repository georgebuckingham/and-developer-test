/**
 *
 * Created by georgebuckingham on 04/05/2016.
 */

module.exports = function(grunt) {
    grunt.initConfig({
        bowercopy: {
            options: {
                srcPrefix: 'bower_components'
            },
            glob: {
                options: {
                    'destPrefix': 'app_client/assets'
                },
                files: {
                    // jQuery
                    'javascripts/jquery.min.js': 'jquery/dist/jquery.min.js',
                    'javascripts/jquery.min.map': 'jquery/dist/jquery.min.map',
                    // Bootstrap
                    'javascripts/bootstrap.min.js': 'bootstrap/dist/js/bootstrap.min.js',
                    'stylesheets/bootstrap.min.css': 'bootstrap/dist/css/bootstrap.min.css',
                    'stylesheets/bootstrap.min.css.map': 'bootstrap/dist/css/bootstrap.min.css.map',
                    // Angular
                    'javascripts/angular.min.js': 'angular/angular.min.js',
                    'javascripts/angular-route.min.js': 'angular-route/angular-route.min.js',
                    'javascripts/angular-route.min.js.map': 'angular-route/angular-route.min.js.map',
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-bowercopy');
    grunt.registerTask('default', ['bowercopy']);
}
