/**
 * 
 * Created by georgebuckingham on 04/05/2016.
 */

angular.module('4sqwhere')
    .factory('SearchService', ['$http', searchService]);

function searchService($http) {
    return {
        search: function(params) {
            var config = {
                params: params
            };
            return $http.get('/api/search', config);
        }
    }
}
