/**
 * 
 * Created by georgebuckingham on 04/05/2016.
 */


function homeCtrl(SearchService) {
    var ctrl = this;
    ctrl.offsetIncrementAmount = 4;
    ctrl.search = {};
    ctrl.results = [];
    ctrl.search.offset = 0;
    ctrl.search.limit = ctrl.offsetIncrementAmount;
    ctrl.errMessage = "";
    ctrl.selectedVenue = null;
    
    ctrl.anyResults = function() {
        return ctrl.results.length > 0;
    }
    
    ctrl.submit = function() {
        ctrl.errMessage = "";
        SearchService.search(ctrl.search)
            .then(function(response) {
                ctrl.results = response.data;
            }, function(errResponse) {
                ctrl.errMessage = errResponse.data.errorDetail;
            });
    };
    
    ctrl.setSelectedVenue = function(venue) {
        ctrl.selectedVenue = venue;
        var latlng = [venue.location.lng, venue.location.lat];
        var map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v8',
            center: latlng,
            zoom: 15,
            // causes pan & zoom handlers not to be applied, similar to
            // .dragging.disable() and other handler .disable() funtions in Leaflet.
            interactive: false
        });
    }
    
    ctrl.incrementOffset = function() {
        ctrl.search.offset += ctrl.offsetIncrementAmount;
        ctrl.submit();
    }
    
    ctrl.decrementOffset = function() {
        ctrl.search.offset -= ctrl.offsetIncrementAmount;
        if (ctrl.search.offset < 0) {
            ctrl.search.offset = 0;
        } else {
            ctrl.submit();
        }
    }
}

angular.module('4sqwhere')
    .controller('HomeCtrl', ['SearchService', homeCtrl]);
