/**
 * 
 * Created by georgebuckingham on 04/05/2016.
 */

angular.module('4sqwhere', ['ngRoute'])
    .config(['$routeProvider', routeProvider]);

function routeProvider($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '/views/home.view.html',
            controller: 'HomeCtrl',
            controllerAs: 'ctrl'
        })
        .otherwise({
            redirectTo: '/'
        });
}
