/**
 * 
 * Created by georgebuckingham on 04/05/2016.
 */

describe('HomeCtrl', function() {
    beforeEach(module('4sqwhere'));
    
    var ctrl, searchService, mockBackend;
    
    beforeEach(inject(function($controller, SearchService, $httpBackend) {
        mockBackend = $httpBackend;
        
        spyOn(SearchService, 'search').and.callThrough();
        
        searchService = SearchService;
        ctrl = $controller('HomeCtrl', ['SearchService']);
        spyOn(ctrl, 'submit').and.callThrough();
    }));
    
    it('should run search function on SearchService when form is submitted', function() {
        ctrl.search = {
            term: "Soho, London"
        };
        ctrl.submit();
        expect(searchService.search).toHaveBeenCalled();
        expect(searchService.search.calls.count()).toBe(1);
    });
    
    it('should increment the offset by `offsetDifference` when incrementOffset() is run', function() {
        expect(ctrl.search.offset).toEqual(0);
        ctrl.incrementOffset();
        expect(ctrl.search.offset).toEqual(0 + ctrl.offsetIncrementAmount);
    });
    
    it('should decrement the offset by `offsetDifference` when decrementOffset() is run', function() {
        expect(ctrl.search.offset).toEqual(0);
        ctrl.incrementOffset();
        ctrl.incrementOffset();
        ctrl.decrementOffset();
        expect(ctrl.search.offset).toEqual(0 + ctrl.offsetIncrementAmount);
        ctrl.decrementOffset();
        ctrl.decrementOffset();
        expect(ctrl.search.offset).toEqual(0);
    });
    
    it('should run the submit function when offset is incremented', function() {
        expect(ctrl.submit).not.toHaveBeenCalled();
        ctrl.incrementOffset();
        expect(ctrl.submit).toHaveBeenCalled();
        expect(ctrl.submit.calls.count()).toBe(1);
        ctrl.incrementOffset();
        expect(ctrl.submit.calls.count()).toBe(2);
    });
    
    it('should run the submit function when offset is decremented', function() {
        expect(ctrl.submit).not.toHaveBeenCalled();
        ctrl.decrementOffset();
        expect(ctrl.submit).not.toHaveBeenCalled();
        ctrl.incrementOffset();
        ctrl.incrementOffset();
        ctrl.decrementOffset();
        expect(ctrl.submit).toHaveBeenCalled();
        expect(ctrl.submit.calls.count()).toBe(3);
    });
    
    it('should determine whether there are any results', function() {
        expect(ctrl.anyResults()).toEqual(false);
        ctrl.results = [1];
        expect(ctrl.anyResults()).toEqual(true);
    });
    
    it('should set error message to empty on new search', function() {
        expect(ctrl.errMessage).toEqual('');
        ctrl.errMessage = 'Hello';
        expect(ctrl.errMessage).toBe('Hello');
        ctrl.submit();
        expect(ctrl.errMessage).toBe('');
    });
    
    it('should set errMessage based on an error response', function() {
        mockBackend.expectGET(/.*?api\/search?.*/g)
            .respond(400,
                {
                    errorDetail: "ErrorMessage"
                }
            );
        expect(ctrl.errMessage).toBe('');
        ctrl.submit();
        mockBackend.flush();
        expect(ctrl.errMessage).toBe('ErrorMessage');
        mockBackend.verifyNoOutstandingExpectation();
        mockBackend.verifyNoOutstandingRequest();
    })
    
});
