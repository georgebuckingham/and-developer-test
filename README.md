# ANDigital Developer Test - “4sqwhere?”

George Buckingham <george@georgebuckingham.co.uk>

## Dependencies

* Git
* Node (4.4.3 LTS)

## Installation instructions

* `git clone` the repository, or download a ZIP of the master branch
* Navigate to the download directory
    * Rename `config.sample.json` to `config.json`
    * Add foursquare `client_id` and `client_secret` where indicated
    * Run `npm run dev`
    * Navigate to http://localhost:3000

## Approach taken

* Node / express
    * Allows me to hide api keys/secret out of public source control repository and from being open to the client
    * I could potentially use a different venue discovery service instead of foursquare without updating the front end of the app
* Angular
    * Seamless model binding, single page app
    * Prevents constant `$().html()` style jQuery constructs
    * Declarative
* Bootstrap
    * Responsive (though I ran out of time to make a proper mobile interface)
    * Widely known
* Grunt
    * Fairly simple requirement, grunt is quick to set up
* Jasmine / Karma
    * Run mocked front-end tests in a browser
    * Can use same assets as the app, so testing latest version

## Usage instructions

In case it's not so clear (I would’ve loved to put a bit of polish on the UI but started to run out of time):

1. Open home page
1. Enter a search term into the text field
    * e.g. 'Soho, London'
1. Wait for a list of venues to load beneath the search field
1. Navigate left/right using the arrow buttons
1. Click on a venue name to load its details and an indication of its location

## Other ideas I wanted to try, and will over the next few days

* Add a heatmap of the most checked-in locations in the area
* Run a second search to the 'trending' API, to get information of large events happening near by
    * Connect this to the Citymapper API to get a display like 'x event is happening nearby, get there in 15 minutes by taking the .. to ..'
* Much improved UX!
* More tests :)
